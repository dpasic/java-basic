package com.ericsson.io;

import java.io.IOException;

public interface IOOperationable {

    /**
     * @param path
     * @return message as a String
     * @throws IOException
     */
    String read(String path) throws IOException;

    /**
     * @param path
     * @param message
     * @throws IOException
     */
    void write(String path, String message) throws IOException;
}
