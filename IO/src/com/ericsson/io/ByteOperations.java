package com.ericsson.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ByteOperations extends AbstractOperations {

    // get the instance used for Singleton Design Pattern
    public static AbstractOperations getInstance() {
        // Lazy Loading Design Pattern
        // not thread-safe (should use synchronized block or Eager Loading)
        if (instance == null) {
            instance = new ByteOperations();
        }
        return instance;
    }

    @Override
    public String read(String path) throws IOException {
        // used for building Strings (it doesn't create a new String for every concatenation)
        StringBuilder sb = new StringBuilder();

        // FileInputStream reads data byte by byte
        // implements Closeable interface
        try (InputStream is = new FileInputStream(path)) {
            System.out.println("===READ LOG===");
            int dataByte;
            // read method returns -1 if EOF (end-of-file)
            while ((dataByte = is.read()) != -1) {
                // must be casted to get char value
                char dataChar = (char) dataByte;
                System.out.print(dataChar);
                sb.append(dataChar);
            }
            System.out.printf("%n==============%n");
            return sb.toString();
        }
    }

    @Override
    public void write(String path, String message) throws IOException {
        // FileOutputStream writes data byte by byte
        // implements Closeable interface
        try (OutputStream os = new FileOutputStream(path)) {
            os.write(message.getBytes());
        }
    }

}
