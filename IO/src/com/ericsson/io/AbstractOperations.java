package com.ericsson.io;

public abstract class AbstractOperations implements IOOperationable {

	// protected instance field used for Singleton Design Pattern
    protected static AbstractOperations instance;

    // protected constructor used for Singleton Design Pattern
    protected AbstractOperations() {}

}
