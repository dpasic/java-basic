package com.ericsson.io;

import java.io.IOException;

public class IOOperationsClient {

    // relative path to the project root folder
    private static final String PATH = "test_file.txt";

    private static final String MESSAGE = String.format("This is a content of my test file.%nA second line.");

    public static void main(String[] args) {
        try {
            System.out.println("BYTE STREM");
            writeReadMessage(IOOperation.BYTE);
            System.out.println("CHARACTER STREM");
            writeReadMessage(IOOperation.CHARACTER);
            System.out.println("BUFFERED STREM");
            writeReadMessage(IOOperation.BUFFERED);
            System.out.println("FILES NIO");
            writeReadMessage(IOOperation.FILES);

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void writeReadMessage(IOOperation operation) throws IOException {
        // Simple Factory Design Pattern
        IOOperationable io = IOOperationsFactory.getInstance(operation);

        io.write(PATH, MESSAGE);
        // the "var" keyword (since Java 10) detects automatically the data type of a variable based on the surrounding context
        var message = io.read(PATH);

        System.out.printf("Message:%n%s%n%n", message);
    }

}
