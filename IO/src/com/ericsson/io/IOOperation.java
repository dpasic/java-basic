package com.ericsson.io;

public enum IOOperation {

    BYTE,
    CHARACTER,
    BUFFERED,
    FILES;
}
