package com.ericsson.io;

// a final class can't be extended
public final class IOOperationsFactory {

	// don't let anyone instantiate this class
	// the class works the same way as built-in Math class
	private IOOperationsFactory() {
	}

	// every method present inside the final class is always final by default
	// a final method can't be overridden
	public static IOOperationable getInstance(IOOperation operation) {
		// the New switch Expression (since Java 13)
		return switch (operation) {
		// in each case, Singleton Design Pattern is used
		case BYTE -> ByteOperations.getInstance();
		case CHARACTER -> CharacterOperations.getInstance();
		case BUFFERED -> BufferedOperations.getInstance();
		case FILES -> FilesOperations.getInstance();
		// 'default' branch is unnecessary when using the switch Expression
		// default -> throw new IllegalArgumentException("Unexpected value: " + operation);
		};
	}
}
