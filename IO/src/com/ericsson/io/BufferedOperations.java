package com.ericsson.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedOperations extends AbstractOperations {

    // get the instance used for Singleton Design Pattern
    public static AbstractOperations getInstance() {
        // Lazy Loading Design Pattern
        // not thread-safe (should use synchronized block or Eager Loading)
        if (instance == null) {
            instance = new BufferedOperations();
        }
        return instance;
    }

    @Override
    public String read(String path) throws IOException {
        // used for building Strings (it doesn't create a new String for every concatenation)
        StringBuilder sb = new StringBuilder();

        // BufferedReader reads data line by line
        // implements Closeable interface
        // Decorator Design Pattern
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            System.out.println("===READ LOG===");
            String line;
            // readLine method returns null if EOF (end-of-file)
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                // Builder Design Pattern by using Fluent API
                sb.append(line)
                        // a new line has to be appended because readLine is used
                        .append(String.format("%n"));
            }
            System.out.println("==============");
            return sb.toString();
        }
    }

    @Override
    public void write(String path, String message) throws IOException {
        // BufferedWriter writes data line by line
        // implements Closeable interface
        // Decorator Design Pattern
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            writer.write(message);
        }
    }

}
