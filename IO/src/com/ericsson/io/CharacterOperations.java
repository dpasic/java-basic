package com.ericsson.io;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class CharacterOperations extends AbstractOperations {

    // get the instance used for Singleton Design Pattern
    public static AbstractOperations getInstance() {
        // Lazy Loading Design Pattern
        // not thread-safe (should use synchronized block or Eager Loading)
        if (instance == null) {
            instance = new CharacterOperations();
        }
        return instance;
    }

    @Override
    public String read(String path) throws IOException {
        // used for building Strings (it doesn't create a new String for every concatenation)
        StringBuilder sb = new StringBuilder();

        // FileReader reads data character by character
        // implements Closeable interface
        try (Reader reader = new FileReader(path)) {
            System.out.println("===READ LOG===");
            int dataByte;
            // read method returns -1 if EOF (end-of-file)
            while ((dataByte = reader.read()) != -1) {
                // must be casted to get char value
                char dataChar = (char) dataByte;
                System.out.print(dataChar);
                sb.append(dataChar);
            }
            System.out.printf("%n==============%n");
            return sb.toString();
        }
    }

    @Override
    public void write(String path, String message) throws IOException {
        // FileWriter writes data character by character
        // implements Closeable interface
        try (Writer writer = new FileWriter(path)) {
            writer.write(message);
        }
    }

}
