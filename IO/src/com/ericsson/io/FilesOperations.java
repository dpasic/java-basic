package com.ericsson.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FilesOperations extends AbstractOperations {

    // get the instance used for Singleton Design Pattern
    public static AbstractOperations getInstance() {
        // Lazy Loading Design Pattern
        // not thread-safe (should use synchronized block or Eager Loading)
        if (instance == null) {
            instance = new FilesOperations();
        }
        return instance;
    }
	
	@Override
	public String read(String path) throws IOException {
		// reads all content from a file into a string (since Java 11)
		String content = Files.readString(Paths.get(path));
		System.out.printf("===READ LOG===%n%s%n==============%n", content);
		return content;
	}

	@Override
	public void write(String path, String message) throws IOException {
		// write a CharSequence to a file (since Java 11)
		Files.writeString(Paths.get(path), message);
	}

}
