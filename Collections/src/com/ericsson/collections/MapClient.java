package com.ericsson.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapClient {

    public static void main(String[] args) {
        // contains <key, value> pairs
        // Map is an interface and HashMap is an implementation
        Map<Integer, List<Book>> mapBooks = new HashMap<>();

        int[] keys = { 1, 2 };

        if (!mapBooks.isEmpty()) {
            System.out.println("Already contains elements!");
            return;
        }

        // books on key 1
        if (!mapBooks.containsKey(keys[0])) {
            mapBooks.put(keys[0], new ArrayList<>());
        }
        List<Book> books = mapBooks.get(keys[0]);
        books.add(new Book("Harry Potter", "J. K. Rowling", 600));
        books.add(new Book("Game of Thrones", "George R. R. Martin", 700));

        // books on key 2
        if (!mapBooks.containsKey(keys[1])) {
            mapBooks.put(keys[1], new ArrayList<>());
        }
        Book fiftyShadesBook = new Book("Fifty Shades of Grey", "E. L. James", 500);
        if (!mapBooks.get(keys[1]).contains(fiftyShadesBook)) {
            mapBooks.get(keys[1]).add(fiftyShadesBook);
        }

        printKeys(mapBooks);
        printValues(mapBooks);
    }

    // uses keySet method to iterate over keys
    private static void printKeys(Map<Integer, List<Book>> mapBooks) {
        System.out.println("KEYS:");
        for (int key : mapBooks.keySet()) {
            System.out.printf("On key: {%d} is value: %s%n", key, mapBooks.get(key));
        }
    }

    // uses values method to iterate over values
    private static void printValues(Map<Integer, List<Book>> mapBooks) {
        System.out.println("VALUES:");
        for (List<Book> books : mapBooks.values()) {
            books.forEach(book -> System.out.println(book));
        }
    }

}
