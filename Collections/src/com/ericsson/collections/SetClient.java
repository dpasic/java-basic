package com.ericsson.collections;

import java.util.HashSet;
import java.util.Set;

public class SetClient {

    public static void main(String[] args) {
        // a unique set of items (uses hashCode and equals methods defined in an object)
        // Set is an interface and HashSet is an implementation
        Set<Book> set = new HashSet<>();

        if (set.isEmpty()) {
            set.add(new Book("Harry Potter", "J. K. Rowling", 600));
            set.add(new Book("Game of Thrones", "George R. R. Martin", 700));
            set.add(new Book("Harry Potter", "J. K. Rowling", 600));
            set.add(new Book("Fifty Shades of Grey", "E. L. James", 500));
            set.add(new Book("Game of Thrones", "George R. R. Martin", 700));
        }

        System.out.printf("Books count: %d%n", set.size());
        //        for (Book book : set) {
        //            System.out.println(book);
        //        }

        // lambda
        set.forEach(book -> System.out.println(book));
    }

}
