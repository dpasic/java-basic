package com.ericsson;

import java.util.Arrays;
import java.util.Random;

public class ArraysRandom {

    // the variable has to be static because is used in a static reference
    static final int GRADES_COUNT = 5;

    public static void main(String[] args) {

        // define an array with a constant
        int[] grades = new int[GRADES_COUNT];

        // instantiate a random generator
        Random rand = new Random();

        for (int i = 0; i < GRADES_COUNT; i++) {
            // rand.nextInt(5) generates number between 0 and 4
            grades[i] = rand.nextInt(5) + 1;
        }

        System.out.println("Before sort:");
        printGrades(grades);

        // sort the array
        Arrays.sort(grades);

        System.out.println("\nAfter sort:");
        printGrades(grades);
    }

    /**
     * print grades by using for-each loop
     *
     * @param randomGrades
     */
    private static void printGrades(int[] randomGrades) {
        for (int grade : randomGrades) {
            System.out.println(grade);
        }
    }

}
