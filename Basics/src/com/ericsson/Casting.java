package com.ericsson;

public class Casting {

	public static void main(String[] args) {

		int sum = 5 + 4 + 3;
		int count = 3;
		
		// explicit cast
		double average = (double)sum / count;

		System.out.println(average);
	}

}
