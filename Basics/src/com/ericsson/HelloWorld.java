// a package name where a file is placed in
package com.ericsson;

// a class name has to be the same as a file name where it is placed
public class HelloWorld {

    // the starting point of every console application
    public static void main(String[] args) {

        // console print out
        System.out.println("Hello World!");
    }

}
