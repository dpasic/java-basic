package com.ericsson;

public class ConditionalStatement {

    public static void main(String[] args) {

        int grade = 5;

        // if else statement
        if (grade > 4) {
            System.out.println("Excellent");
        } else if (grade < 2) {
            System.out.println("Insufficient");
        } else {
            System.out.println("Between 2 and 4");
        }

        // switch statement
        switch (grade) {
        case 5:
            System.out.println("Excellent");
            break;
        case 1:
            System.out.println("Insufficient");
            break;
        default:
            System.out.println("Between 2 and 4");
            break;
        }

        // ternary operator
        String result = grade > 1 ? "Passed" : "Failed";
        System.out.println(result);
    }

}
