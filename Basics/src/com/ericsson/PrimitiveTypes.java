package com.ericsson;

public class PrimitiveTypes {

    public static void main(String[] args) {

        // primitive types definitions
        char firstName = 'Đ';
        char firstSurname = 'P';
        char newLine = '\n';
        int number = 7;

        // declaration
        boolean truth;
        // initialization
        truth = true;

        // print out "User ĐP has a favorite number 7. \nIt is true."
        System.out.print("User " + firstName + firstSurname + " has a favorite number " + number + ".");
        System.out.print(newLine + "It is " + truth + ".");
    }

}
