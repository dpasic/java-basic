package com.ericsson;

import java.util.ArrayList;
import java.util.Scanner;

public class ListsEnums {

    // enum values are usually written in uppercase
    enum Type {
        CITY,
        // the terminating semicolon is needed if you add some code to the enum
        COUNTRY;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        // instantiate a new ArrayList
        ArrayList<String> strings = new ArrayList<>();

        System.out.println("What do you want to fill (cities/countries)?");
        // if we use scan.next(),
        // in the stream stays enter ('\n') which can be flushed by scan.nextLine()
        // if we always use nextLine, we omit the flushing problems
        // (but we can't make a constraint for reading only one word)
        String input = in.nextLine();

        // first type hard-coded string because the string use for comparison (input) can be null
        Type enumType = "cities".equals(input) ? Type.CITY : Type.COUNTRY;

        switch (enumType) {
        case CITY:
            fillCities(strings);
            break;
        case COUNTRY:
            fillCountries(strings, in);
            break;
        }

        // free up the resources
        in.close();

        printStrings(strings);

        // remove the first string by value
        strings.remove(strings.get(0));

        printStrings(strings);
    }

    /**
     * @param cities
     */
    private static void fillCities(ArrayList<String> cities) {
        cities.add("Zagreb");
        cities.add(" New York");
        cities.add(" Paris ");
    }

    /**
     * @param countries
     * @param in
     */
    private static void fillCountries(ArrayList<String> countries, Scanner in) {
        // enter 3 countries by using the constant instead of "magic number" in the for loop
        final int countriesCount = 3;

        for (int i = 1; i <= countriesCount; i++) {
            System.out.printf("Enter %d. country: ", i);
            countries.add(in.nextLine());
        }
    }

    /**
     * print strings and the size of the array
     *
     * @param strings
     */
    private static void printStrings(ArrayList<String> strings) {
        // printf uses print + String.format
        // %d - Decimal Integer
        // %n - Platform-specific line separator
        // reference: https://dzone.com/articles/java-string-format-examples
        System.out.printf("The size of the array is %d%n", strings.size());
        for (String string : strings) {
            // trim the string (removes white-spaces from the start and the end)
            System.out.println(string.trim());
        }
    }

}
