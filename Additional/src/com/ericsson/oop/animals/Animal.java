package com.ericsson.oop.animals;

public class Animal implements Comparable<Animal> {

    private String name;
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Animal other) {
        // a helper method on Integer for comparing (wrapper around int)
        return Integer.compare(age, other.age);
    }

    @Override
    public String toString() {
        return String.format("%s %s is %d years old.", getClass().getSimpleName(), name, age);
    }
}
