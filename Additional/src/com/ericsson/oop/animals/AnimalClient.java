package com.ericsson.oop.animals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AnimalClient {

    public static void main(String[] args) {
        List<Animal> animals = Arrays.asList(
                new Cat("Oliver", 5),
                new Dog("Jack", 2),
                new Parrot("Harry", 15));

        System.out.println("BEFORE SORT:");
        // polymorphism is used for printing Animals
        animals.forEach(System.out::println);

        Collections.sort(animals);

        System.out.println("AFTER SORT:");
        animals.forEach(System.out::println);
    }

}
