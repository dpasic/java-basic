package com.ericsson.oop.students;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Exam {

    private String name;
    private LocalDateTime term;

    public Exam(String name, LocalDateTime term) {
        this.name = name;
        this.term = term;
    }

    @Override
    public String toString() {
        // term is formatted by using DateTimeFormatters pattern
        return String.format("%s at %s", name, term.format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")));
    }
}
