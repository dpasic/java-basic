package com.ericsson.oop.students;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;

public class StudentClient {

    public static void main(String[] args) {
        Student s1 = new Student("Oliver", "Smith");
        Student s2 = new Student("Jack", "Jones");

        // LocalDateTime.of returns a LocalDateTime object
        Exam e1 = new Exam("Java", LocalDateTime.of(2019, Month.OCTOBER, 1, 10, 30));
        Exam e2 = new Exam("Python", LocalDateTime.of(2019, Month.OCTOBER, 1, 12, 0));
        Exam e3 = new Exam("C++", LocalDateTime.of(2019, Month.OCTOBER, 1, 13, 30));

        s1.signExam(e1);
        s1.signExam(e2);
        s2.signExam(e3);

        Arrays.asList(s1, s2).forEach(System.out::println);
    }

}
