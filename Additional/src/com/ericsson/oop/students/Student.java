package com.ericsson.oop.students;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private String firstName;
    private String lastName;
    private List<Exam> exams;

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.exams = new ArrayList<>();
    }

    public void signExam(Exam e) {
        exams.add(e);
    }

    @Override
    public String toString() {
        return String.format("Student %s %s has signed %s.", firstName, lastName, exams);
    }
}
