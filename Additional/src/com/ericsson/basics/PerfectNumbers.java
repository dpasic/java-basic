package com.ericsson.basics;

public class PerfectNumbers {

    public static void main(String[] args) {
        // Underscores in Numeric Literals (since Java 7)
        for (int i = 1; i <= 10_000; i++) {
            if (isPerfect(i)) {
                System.out.println(i);
            }
        }
    }

    private static boolean isPerfect(int number) {
        int dividersSum = 0;
        for (int i = 1; i < number; i++) {
            if (number % i == 0) {
                dividersSum += i;
            }
        }

        return dividersSum == number;
    }

}
