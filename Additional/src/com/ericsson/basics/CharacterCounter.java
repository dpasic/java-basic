package com.ericsson.basics;

import java.util.Scanner;

public class CharacterCounter {

    public static void main(String[] args) {
        int letterCount = 0;
        int digitCount = 0;
        int otherCount = 0;

        // Scanner implements Closeable
        try (Scanner in = new Scanner(System.in)) {
            System.out.println("Enter sentence:");
            String sentence = in.nextLine();

            // for-each doesn't work on Strings, so first we need to get char array
            for (char c : sentence.toCharArray()) {
                // a helper method on Character (wrapper around char)
                if (Character.isLetter(c)) {
                    letterCount++;
                } else if (Character.isDigit(c)) {
                    digitCount++;
                } else {
                    otherCount++;
                }
            }

            System.out.println("Letters: " + letterCount);
            System.out.println("Digits: " + digitCount);
            System.out.println("Other characters: " + otherCount);
        }
    }

}
