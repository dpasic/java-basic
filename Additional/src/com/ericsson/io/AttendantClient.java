package com.ericsson.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class AttendantClient {

	private static final String PATH = "attendants.csv";

	public static void main(String[] args) {
		try {
			List<String> contentLines = Files.readAllLines(Paths.get(PATH));
			
			for (String line : contentLines) {
				Attendant a = Attendant.parse(line);
				System.out.println(a);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
