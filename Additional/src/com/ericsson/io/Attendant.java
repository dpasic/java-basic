package com.ericsson.io;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Attendant {

    private String firstName;
    private String lastName;
    private int yearsOfExperience;
    private LocalDateTime term;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public LocalDateTime getTerm() {
        return term;
    }

    public void setTerm(LocalDateTime term) {
        this.term = term;
    }

    @Override
    public String toString() {
        return String.format("%s %s with %d years of experience is at %s", firstName, lastName, yearsOfExperience,
                term.format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")));
    }
    
    public static Attendant parse(String line) {
    	// split the line by comma (comma separated file is used)
        String[] data = line.split(",");

        // create a new Attendant and set its value by parsing data
        Attendant a = new Attendant();
        a.setFirstName(data[0]);
        a.setLastName(data[1]);
        a.setYearsOfExperience(Integer.parseInt(data[2]));
        a.setTerm(LocalDateTime.parse(data[3], DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm")));

        return a;
    }
}
