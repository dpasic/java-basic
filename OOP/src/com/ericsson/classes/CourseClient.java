package com.ericsson.classes;

public class CourseClient {

    // "psvm" shortcut to generate main method
    public static void main(String[] args) {

        // instantiate a new Course
        Course myCourse = new Course("Java Basic");

        // call methods on the instance
        System.out.println(myCourse.getName());
        myCourse.printName();

        // call static method defined on the Course
        System.out.printf("The number of created instances: %d%n", Course.getId());
    }

}
