package com.ericsson.classes.abstracts;

// static imports (static fields and static methods can be imported)
// if you want to import all members of Math class (import static java.lang.Math.*;)
import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Circle extends GeometricalFigure {

    private int r;

    public Circle(String color, int r) {
        // calls super constructor and adds radius value
        super(color);
        this.r = r;
    }

    @Override
    public double area() {
        // static import is used for Math.pow and Math.PI
        return pow(r, 2) * PI;
    }

    @Override
    public double perimeter() {
        // static import is used for Math.PI
        return 2 * r * PI;
    }
}
