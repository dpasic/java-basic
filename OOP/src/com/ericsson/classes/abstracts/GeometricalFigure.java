package com.ericsson.classes.abstracts;

// "abstract" is a keyword for making a class abstract
// the class can't be instantiated
public abstract class GeometricalFigure {

    private String color;

    public GeometricalFigure(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        // returns a class name without package name by using reflection
        return getClass().getSimpleName();
    }

    // if at least one abstract method, a class has to be abstract
    public abstract double area();

    public abstract double perimeter();
}
