package com.ericsson.classes.abstracts;

public class Rectangle extends GeometricalFigure {

    private int a;
    private int b;

    public Rectangle(String color, int a, int b) {
        // calls super constructor and adds values
        super(color);
        this.a = a;
        this.b = b;
    }

    @Override
    public double area() {
        return a * b;
    }

    @Override
    public double perimeter() {
        return 2 * (a + b);
    }
}
