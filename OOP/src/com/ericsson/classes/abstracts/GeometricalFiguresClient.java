package com.ericsson.classes.abstracts;

import java.util.Arrays;
import java.util.List;

public class GeometricalFiguresClient {

    public static void main(String[] args) {
        // Arrays.asList is helper method on Arrays which is used for creating a list from items
        List<GeometricalFigure> figures = Arrays.asList(
                new Circle("red", 15),
                new Circle("blue", 6),
                new Rectangle("green", 5, 6),
                new Rectangle("yellow", 3, 9));

        // for-each over geometrical figures and call theirs abstract methods
        // %.2f rounds decimal number on 2 digits precision
        for (GeometricalFigure figure : figures) {
            System.out.printf("Area of %s %s is %.2f%n", figure.getColor(), figure, figure.area());
            System.out.printf("Perimeter of %s %s is %.2f%n", figure.getColor(), figure, figure.perimeter());
        }
    }
}
