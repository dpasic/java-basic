package com.ericsson.classes.object;

public class CatClient {

    public static void main(String[] args) {

        Cat cat1 = new Cat(Gender.FEMALE, "white");
        Cat cat2 = new Cat(Gender.FEMALE, "white");

        System.out.println(cat1/* .toString() */);
        System.out.println(cat2);

        // operators can't be overridden in Java
        System.out.println(cat1 == cat2);
        // equals method is overridden
        System.out.println(cat1.equals(cat2));
    }

}
