package com.ericsson.classes.object;

import java.util.Objects;

public class Cat {

    // final fields don't need to be defined if values are set in the constructor
    private final Gender gender;
    private final String color;

    public Cat(Gender gender, String color) {
        this.gender = gender;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Cat { gender=%s, color=%s}", gender, color);
    }

    // equals method should be overridden together with hashCode method
    // it is called when comparing objects (except comparing with "==")
    // if is used with Hash implementations, it compares members in the bucket (after the hashCode is done)
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!(other instanceof Cat)) {
            return false;
        }

        Cat cat = (Cat) other;
        return gender == cat.gender &&
                color.equals(cat.color);
    }

    // all equals method members have to be included in hashCode method
    // used as the bucket number for storing elements of the set/map
    @Override
    public int hashCode() {
        return Objects.hash(gender, color);
    }
}
