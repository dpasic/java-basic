package com.ericsson.classes.inheritance;

public class SportsCar extends Car {

    private double topSpeed;

    // extended class should have all parameters from its parent (Car) and optional additional parameters (topSpeed)
    public SportsCar(String brand, String model, double topSpeed) {
        // super constructor (Car) must be called first
        super(brand, model);
        this.topSpeed = topSpeed;
    }

    // compile annotation (prevents errors in methods naming)
    @Override
    public void start() {
        // super.start() executes start method from its parent (Car)
        System.out.println("Sports car starts");
    }

    public void speedUp() {
        System.out.println("Sports car speeds up");
    }

    @Override
    public String toString() {
        return String.format("%s\tTop speed: %f", super.toString(), topSpeed);
    }
}
