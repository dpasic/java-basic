package com.ericsson.classes.inheritance;

public class Car {

    // private fields
    private String brand;
    private String model;

    // constructor overload
    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    // getters and setters
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    // public methods
    public void start() {
        System.out.println("Car starts");
    }

    public void stop() {
        System.out.println("Car stops");
    }

    // override toString method defined on Object
    // default toString method is (fully qualified class name + hash code)
    @Override
    public String toString() {
        return String.format("Car brand: %s and model: %s", brand, model);
    }
}
