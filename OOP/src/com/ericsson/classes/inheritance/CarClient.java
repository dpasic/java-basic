package com.ericsson.classes.inheritance;

import java.util.ArrayList;

public class CarClient {

    public static void main(String[] args) {
        Car c = new Car("Peugeot", "308");
        c.start();
        c.stop();
        System.out.println("-------------------------------");

        SportsCar sc = new SportsCar("Porsche", "911", 300);
        sc.start();
        sc.speedUp();
        sc.stop();
        System.out.println("-------------------------------");

        // promotion (implicit cast)
        Car car2 = sc;
        // prints out "Sports car starts" because of polymorphism
        car2.start();
        car2.stop();
        System.out.println("-------------------------------");

        // check if car2 is SportsCar
        if (car2 instanceof SportsCar) {
            // explicit cast
            SportsCar sportsCar2 = (SportsCar) car2;
            sportsCar2.speedUp();
        }
        System.out.println("-------------------------------");

        ArrayList<Car> cars = new ArrayList<>();
        // you can add Car and SportsCar because of polymorphism
        cars.add(c);
        cars.add(sc);

        print(c);
        print(sc);
    }

    private static void print(Car c) {
        System.out.println(c);
    }
}
