package com.ericsson.classes;

/**
 * @author edjapas
 */
public class Course {

    // private static field
    // stores value on the class level (every instance will have the same ID value)
    private static int id = 0;

    // name is encapsulated
    // can be accessed by client by using getter and setter public methods
    private String name;

    /**
     * constructor overload #1
     */
    public Course() {
        // increment the ID after a new instance is created
        id++;
    }

    /**
     * constructor overload #2
     *
     * @param name
     */
    public Course(String name) {
        // call the Course constructor without parameters (constructor overload #1)
        // the constructor must be called first
        this();

        this.name = name;
    }

    /**
     * get the ID
     * can be called directly on the class (Course.getId()) or on an instance (c.getId())
     *
     * @return id
     */
    public static int getId() {
        return id;
    }

    /**
     * getter
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * print course's name
     */
    public void printName() {
        System.out.printf("The course name is %s%n", name);
    }
}
