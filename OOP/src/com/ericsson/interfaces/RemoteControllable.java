package com.ericsson.interfaces;

// an interface can extend N interfaces
public interface RemoteControllable extends Switchable {

    void mute();
}
