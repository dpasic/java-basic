package com.ericsson.interfaces;

import java.util.ArrayList;
import java.util.List;

public class SwitchableClient {

    public static void main(String[] args) {
        // list of Switchables (each class which implements Switchable interface)
        List<Switchable> switchables = new ArrayList<>();

        Switchable light = new Light();
        switchables.add(light);

        switchables.add(new AirConditioner());
        // every RemoteControllable is Switchable
        switchables.add(new TVRemoteController());

        // anonymous interface implementation
        switchables.add(new Switchable() {

            @Override
            public void switchOn() {
                System.out.println("Anonymous switches on");
            }

            @Override
            public void switchOff() {
                System.out.println("Anonymous switches off");
            }
        });

        // for-each over switchables and call theirs abstract methods
        for (Switchable s : switchables) {
            // switchable knows in which class does it belong
            System.out.println(s.getClass());
            s.switchOn();

            // check if switchable is RemoteControllable
            // and cast it using pattern variable (since Java 14)
            if (s instanceof RemoteControllable remote) {
                remote.mute();
            }

            s.switchOff();
        }
    }

}
