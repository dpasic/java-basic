package com.ericsson.interfaces.comparable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PersonClient {

    public static void main(String[] args) {
        List<Person> persons = Arrays.asList(
                new Person("Oliver", "Smith", 178),
                new Person("Jack", "Jones", 172),
                new Person("Harry", "Williams", 186));

        System.out.println("Before sort:");
        printPersons(persons);

        // Collections.sort method is sorting the elements of List in ascending order
        Collections.sort(persons);

        System.out.println("After sort:");
        printPersons(persons);
    }

    private static void printPersons(List<Person> persons) {
        //        for (Person p : persons) {
        //            System.out.println(p);
        //        }

        // lambda
        persons.forEach(p -> {
            System.out.println(p);
        });

        // method reference
        //        persons.forEach(System.out::println);
    }
}
