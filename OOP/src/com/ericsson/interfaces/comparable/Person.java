package com.ericsson.interfaces.comparable;

public class Person implements Comparable<Person> {

    private String firstName;
    private String lastName;
    private int height;

    public Person(String firstName, String lastName, int height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
    }

    // the method from Comparable interface
    @Override
    public int compareTo(Person other) {
        //        if (height > other.height) {
        //            return 1;
        //        } else if (height < other.height) {
        //            return -1;
        //        } else {
        //            return 0;
        //        }

        // if we multiply the result with -1, we will change the sorting order (ASC to DESC)
        return Integer.compare(height, other.height);
    }

    @Override
    public String toString() {
        return "Person [firstName=" + firstName + ", lastName=" + lastName + ", height=" + height + "]";
    }

}
