package com.ericsson.throwable;

public class ExceptionExample {

	public static void main(String[] args) {
		try {
			// divide by zero
			int result = 5 / 0;
			System.out.println(result);

		} catch (Exception e) {
			System.out.printf("Exception message: %s%n", e.getMessage());

			// finally is optional
		} finally {
			System.out.println("After try-catch block");
		}

		System.out.println("Done");
	}

}
