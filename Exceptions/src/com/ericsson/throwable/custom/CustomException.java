package com.ericsson.throwable.custom;

// CustomException is a checked exception because it inherits Exception
public class CustomException extends Exception {

    private String user;

    // CustomException constructor contains a private field
    public CustomException(String message, String user) {
        // call super constructor with message parameter
        super(message);
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
