package com.ericsson.throwable.custom;

public class CustomExceptionClient {

    public static void main(String[] args) {
        try {
            // the exception must be handled or re-thrown
            callThrowableMethod();

            // multi-catch block (if ArithmeticException or CustomException then)
        } catch (ArithmeticException | CustomException e) {
            System.out.printf("Exception message: %s%n", e.getMessage());
            if (e instanceof CustomException) {
                System.out.printf("Exception user: %s%n", ((CustomException) e).getUser());
            }

            // the least specific exception (Throwable)
        } catch (Throwable t) {
            System.out.printf("Throwable message: %s%n", t.getMessage());
        }

        System.out.println("Done");
    }

    // the method throws CustomException
    private static void callThrowableMethod() throws CustomException {
        System.out.println("Do some work...");
        throw new CustomException("Custom exception happened", "admin");
    }

}
