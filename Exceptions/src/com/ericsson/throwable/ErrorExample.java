package com.ericsson.throwable;

import java.util.ArrayList;
import java.util.List;

public class ErrorExample {

    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();

        try {
            // infinite loop
            while (true) {
                strings.add("string");
            }

        } catch (Error e) {
            System.out.printf("Error message: %s%n", e.getMessage());

        } finally {
            System.out.println("After try-catch block");
        }

        System.out.println("Done");
    }

}
