package com.ericsson.interfaces.functional;

public class CalculatorClient {

    private static final double A = 5;
    private static final double B = 3;

    public static void main(String[] args) {
        Calculationable addition = new Addition();
        calculate(A, B, addition);

        // anonymous implementation
        //        Calculationable subtraction = new Calculationable() {
        //
        //            @Override
        //            public double calc(double a, double b) {
        //                return a - b;
        //            }
        //        };

        // lambda
        Calculationable subtraction = (a, b) -> a - b;
        calculate(A, B, subtraction);

        calculate(A, B, (a, b) -> a * b);
    }

    private static void calculate(double a, double b, Calculationable calculationable) {
        System.out.printf("Result: %.2f%n", calculationable.calc(a, b));
    }

}
