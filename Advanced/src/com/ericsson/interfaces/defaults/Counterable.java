package com.ericsson.interfaces.defaults;

public interface Counterable {

    // default implementation is provided
    // it can be overridden, but it is not mandatory
    default int count(String s) {
        return s.length();
    }
}
