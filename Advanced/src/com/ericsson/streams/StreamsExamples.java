package com.ericsson.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class StreamsExamples {

    public static void main(String[] args) {
        // for-each through list
        List<String> strings = Arrays.asList(
                "first",
                "second",
                "third");
        strings.forEach(s -> System.out.println(s));

        System.out.println("====================");

        // method reference
        strings.forEach(System.out::println);

        System.out.println("====================");

        // for-each through string
        String myString = "This is a test string";
        myString.chars().forEach(c -> {
            // it has to be casted to char
            System.out.print((char) c);
        });
        System.out.println();
        System.out.println("====================");

        // filter and print values
        List<String> daysInWeek = Arrays.asList(
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday");
        // stream creates a new pipeline which is immutable
        daysInWeek.stream()
                .filter(s -> !s.toLowerCase().startsWith("s"))
                .forEach(System.out::println);

        System.out.println("====================");

        // even numbers from 1 to 10
        IntStream.rangeClosed(1, 10)
                .filter(i -> i % 2 == 0)
                .forEach(System.out::println);
    }

}
